﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Digitals.BL;
using Digitals.ET;

namespace Digitals.Controllers
{
    public class ProductoController : Controller
    {
        // GET: Producto

        private ProductoBL productoBL = new ProductoBL();

        public ActionResult Listar()
        {
            return View(productoBL.Listar());
        }

        public ActionResult Detalles(int id)
        {

            return View(productoBL.Obtener(id));

        }

        public ActionResult Crear(Producto producto)
        {
            return View(producto);
        }

        public ActionResult Editar(int id = 0)
        {
            return View(productoBL.Obtener(id));
        }

        [HttpPost]
        public ActionResult Registrar(Producto producto)
        {
            productoBL.Registrar(producto);
            return Redirect("~/Producto/Listar");
        }

        [HttpPost]
        public ActionResult Actualizar(Producto producto)
        {
            productoBL.Actualizar(producto);
            ViewBag.Mensaje = "Actualizado";
            return Redirect("~/Producto/Listar");
        }

        public ActionResult Eliminar(int id)
        {
            var r = productoBL.Eliminar(id);

            if (!r)
            {
                // Podemos validar para mostrar un mensaje personalizado, por ahora el aplicativo se caera por el throw que hay en nuestra capa DAL
                ViewBag.Mensaje = "Ocurrio un error inesperado";
                return View("~/Views/Shared/_Mensaje.cshtml");
            }

            return Redirect("~/Producto/Listar");
        }
    }

}