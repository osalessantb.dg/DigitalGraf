﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Digitals.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Registrar Cliente";
          

            return View();
        }

        public ActionResult Servicios()
        {
            ViewBag.Message = "Cuenta con nuestros mejores servicios";


            return View();
        }

        public ActionResult Productos()
        {
            ViewBag.Message = "Productos de los mejores";


            return View();
        }
    }
}