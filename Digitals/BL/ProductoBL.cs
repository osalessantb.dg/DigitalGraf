﻿using Digitals.ET;
using Digitals.DAL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;

namespace Digitals.BL
{

    public class ProductoBL
    {
        private ProductoDAL productoDAL = new ProductoDAL();

        public List<Producto> Listar()
        {
            return productoDAL.Listar();
        }
        public Producto Obtener(int id)
        {
            return productoDAL.Obtener(id);
        }

        public bool Actualizar(Producto producto)
        {
            return productoDAL.Actualizar(producto);
        }

        public bool Registrar(Producto producto)
        {
            return productoDAL.Registrar(producto);
        }

        public bool Eliminar(int id)
        {
            return productoDAL.Eliminar(id);
        }
    }

}
