﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitals.ET
{
    public class Producto
    {
        public int IdProducto {get; set;}
        public string Nombre { get; set; }
        public string Detalle { get; set; }
        public int Cantidad { get; set; }
        public double Precio { get; set; }

    }
}