﻿using Digitals.ET;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;

namespace Digitals.DAL
{
    public class ProductoDAL
    {
        private SqlConnection con;

        private void connection()
        {

            //Cadena de Conexión
            string constring = ConfigurationManager.ConnectionStrings["DigitalGrafConn"].ToString();
            con = new SqlConnection(constring);
        }

        public List<Producto> Listar()
        {
            var productos = new List<Producto>();

            try
            {
                connection();

                {
                    con.Open();

                    var query = new SqlCommand("SELECT * FROM PRODUCTO", con);
                    using (var dr = query.ExecuteReader())

                    {
                        while (dr.Read())
                        {
                            // Producto
                            var producto = new Producto
                            {
                                IdProducto = Convert.ToInt32(dr["IdProducto"]),
                                Nombre = dr["Nombre"].ToString(),
                                Detalle = dr["Detalle"].ToString(),
                                Cantidad = Convert.ToInt32(dr["Cantidad"]),
                                Precio = Convert.ToDouble(dr["Precio"]),

                            };

                            // Agregamos el Producto a la lista genérica

                            productos.Add(producto);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                throw;
            }

            return productos;
        }

        public Producto Obtener(int id)
        {
            var producto = new Producto();

            try
            {
                connection();

                {
                    con.Open();

                    var query = new SqlCommand("SELECT * FROM PRODUCTO WHERE IDPRODUCTO = @IDPRODUCTO", con);
                    query.Parameters.AddWithValue("@IDPRODUCTO", id);

                    using (var dr = query.ExecuteReader())
                    {
                        dr.Read();
                        if (dr.HasRows)
                        {
                            producto.IdProducto = Convert.ToInt32(dr["IdProducto"]);
                            producto.Nombre = dr["Nombre"].ToString();
                            producto.Detalle = dr["Detalle"].ToString();
                            producto.Cantidad = Convert.ToInt32(dr["Cantidad"]);
                            producto.Precio = Convert.ToDouble(dr["Precio"]);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }

            return producto;
        }

        public bool Actualizar(Producto producto)
        {
            bool respuesta = false;

            try
            {
                connection();

                {
                    con.Open();

                    var query = new SqlCommand("UPDATE Producto SET Nombre = @p0, Detalle = @p1, Cantidad = @p2, Precio = @p3 WHERE IdProducto = @p4", con);


                    query.Parameters.AddWithValue("@p0", producto.Nombre);
                    query.Parameters.AddWithValue("@p1", producto.Detalle);
                    query.Parameters.AddWithValue("@p2", producto.Cantidad);
                    query.Parameters.AddWithValue("@p3", producto.Precio);
                    query.Parameters.AddWithValue("@p4", producto.IdProducto);

                    query.ExecuteNonQuery();

                    respuesta = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }

            return respuesta;
        }

        public bool Registrar(Producto producto)
        {
            connection();
            {
                var query = new SqlCommand("INSERT INTO PRODUCTO(Nombre, Detalle, Cantidad, Precio) VALUES (@p0, @p1, @p2, @p3)", con);

                query.Parameters.AddWithValue("@p0", producto.Nombre);
                query.Parameters.AddWithValue("@p1", producto.Detalle);
                query.Parameters.AddWithValue("@p2", producto.Cantidad);
                query.Parameters.AddWithValue("@p3", producto.Precio);
                con.Open();
                int i = query.ExecuteNonQuery();
                con.Close();

                if (i >= 1)
                    return true;
                else
                    return false;
            }
        }

        public bool Eliminar(int id)
        {
            bool respuesta = false;

            try
            {
                connection();

                {
                    con.Open();

                    var query = new SqlCommand("DELETE FROM PRODUCTO WHERE IDPRODUCTO = @p0", con);
                    query.Parameters.AddWithValue("@p0", id);

                    query.ExecuteNonQuery();

                    respuesta = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }

            return respuesta;
        }
    }
}