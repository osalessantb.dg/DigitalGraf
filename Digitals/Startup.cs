﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Digitals.Startup))]
namespace Digitals
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
